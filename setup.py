import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="wfp_giuli",
    version="0.1.0",
    author="Giulia Martini",
    author_email="martini.giulia94@gmail.com",
    description="WFP rainfall prediction assignment",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="link",
    packages=setuptools.find_packages(),
    # packages that are needed for pip install by users
    install_requires=[
        "numpy",
        "scipy",
        "statsmodels",
        "pandas",
        "matplotlib",
        "seaborn",
        "tabulate",
    ],
    # additional packages for development (additional to install_requires)
    extras_require={
        "with_jupyterlab": ["jupyterlab"],
        "development": ["pdoc3", "pytest", "coverage", "twine"],
        "testing": ["pytest", "coverage"],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: TBD",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering/Humanitarian",
        "Intended Audience :: Science/Research",
    ],
    python_requires=">=3.6",
)
