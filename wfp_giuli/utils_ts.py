"""
Time-series modelling related utilities.
"""

from datetime import datetime, timedelta

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def get_time_series_data(region_id: int = 100):
    """
    Deprecated!

    """
    # Fetch the data from provided url
    url = "https://wfp-public.oss-eu-central-1.aliyuncs.com/tmp/rainfall_by_country.csv"
    df = pd.read_csv(url)
    df.mean_rainfall = df.mean_rainfall.replace(-1, None)
    # assigning None to negative mean_rainfall values
    df["day"] = df.decade.replace([1, 2, 3], [5, 15, 25])
    # create column 'date'
    df["date"] = pd.to_datetime(df[["year", "month"]].assign(Day=df["day"]))

    # create time series with constant freq
    start_datetime = datetime(year=1990, month=1, day=1, hour=0, minute=0, second=0)
    end_datetime = datetime(year=2019, month=12, day=31, hour=23, minute=59, second=59)

    n_step = 3 * 10 * 12 * 3

    # we need an integer hour frequency as the smallest unit that `statsmodels` can deal with is 'H'
    # this introduces an offset to the middle of month-thirds, the code below minimizes this error
    diff_datetime = end_datetime - start_datetime

    total_hours = diff_datetime.days * 24 + diff_datetime.seconds / 3600
    step_hours = int(round(total_hours / (n_step - 1)))
    rem_time = timedelta(hours=total_hours - step_hours * (n_step - 1))

    updated_start_datetime = start_datetime + rem_time / 2
    day = (rem_time / 2).days
    dti = pd.date_range(updated_start_datetime, periods=n_step, freq=f"{step_hours}H",)

    # set a simple analysis with only one location
    regions = df.AdminCode.unique()
    regions_sub = [regions[region_id]]
    df_analysis = df[df.AdminCode.isin(regions_sub)]
    df_analysis.index = dti
    df_analysis = df_analysis.drop(
        ["AdminCode", "year", "month", "decade", "date"], axis=1
    )
    return df_analysis


def int_to_datetime(
    ts_data: pd.core.series.Series, start_int: int, end_int: int
) -> tuple:
    """
    Convert integer (index) to `datetime`.

    Note: very specific function.

    Args:
        ts_data: times series data
        start_int: start index (integer)
        end_int: end index (integer)

    Returns:
        datetime values corresponding to `start_int` and `end_int`
        frequency of the time series data

    Raises:
        TypeError: if `ts_data` is not `pandas.core.series.Series`

    """
    if not isinstance(ts_data, pd.core.series.Series):
        raise TypeError(
            "`ts_data` has incorrect type. It should be `pandas.core.series.Series`."
        )

    first_datetime = ts_data.index[0]
    freq = first_datetime.freq
    start_datetime = first_datetime + start_int * freq
    end_datetime = first_datetime + end_int * freq
    return start_datetime, end_datetime, freq


def visualize_time_series_fit(
    all_ts_data: pd.core.series.Series,
    ts_data_for_fit: pd.core.series.Series,
    times_pred,
    mean_prediction,
    steps_forecast,
    times_forecast: pd.core.indexes.datetimes.DatetimeIndex,
    confidence_interval_forecast: np.ndarray,
    confidence_interval_alpha: float = 0.05,
    confidence_interval_warning: str = "",
    aic: float = None,
    r_squared: float = None,
):
    # ===================================================
    # PRE-PROCESSING & INIT
    # ===================================================
    y_min = 0
    y_max = np.max(all_ts_data) * 1.10
    col_data = "#1f77b4"
    col_model = "#ff7f0e"
    col_fit_limit = "red"
    fill_alpha = 0.5

    n_dt_fit = len(ts_data_for_fit)

    # Limits for the zoomed-in plot
    start_datetime_zoom, end_datetime_zoom, _ = int_to_datetime(
        ts_data=all_ts_data,
        start_int=n_dt_fit - 2 * steps_forecast,
        end_int=n_dt_fit + steps_forecast,
    )

    # ===================================================
    # PLOT
    # ===================================================
    fig, (ax_top, ax_bot) = plt.subplots(2, 1)

    # ---------------------------------------------------
    # Total time-series with long-shot extrapolation
    # ---------------------------------------------------
    ax_top.fill_between(
        times_forecast,
        confidence_interval_forecast[:, 0],
        confidence_interval_forecast[:, 1],
        facecolor=col_model,
        alpha=fill_alpha,
        label=f"model_name prediction: {100 - confidence_interval_alpha * 100:.0f}% {confidence_interval_warning} confidence interval",
    )
    ax_top.plot(all_ts_data.T.index, all_ts_data.T, label="data", color=col_data)
    ax_top.plot(
        times_pred,
        mean_prediction,
        label="model_name prediction: mean",
        color=col_model,
    )
    ax_top.axvline(ts_data_for_fit.index[-1], c=col_fit_limit)
    if aic:
        ax_top.text(
            all_ts_data.T.index[1], y_max - 0.05 * (y_max - y_min), f"AIC={aic:.3e}"
        )
    if r_squared:
        ax_top.text(
            all_ts_data.T.index[1],
            y_max - 0.10 * (y_max - y_min),
            f"R^2 ={r_squared:.3f}",
        )

    ax_top.set_ylim(y_min, y_max)
    ax_top.set_ylabel("Total rainfall per one-third month [mm]")
    ax_top.set_xlabel("Time (one-third month resolution)")
    ax_top.grid()
    ax_top.legend(loc="upper right")
    ax_top.set_title("Total time-series")

    # ---------------------------------------------------
    # Zoomed-in to the first bit of the extrapolation
    # ---------------------------------------------------
    ax_bot.fill_between(
        times_forecast,
        confidence_interval_forecast[:, 0],
        confidence_interval_forecast[:, 1],
        facecolor=col_model,
        alpha=fill_alpha,
    )
    ax_bot.plot(all_ts_data.T.index, all_ts_data.T, "-o", color=col_data)
    ax_bot.plot(times_pred, mean_prediction, "-o", color=col_model)
    ax_bot.axvline(ts_data_for_fit.index[-1], c=col_fit_limit)

    ax_bot.set_ylim(y_min, y_max)
    ax_bot.set_xlim(start_datetime_zoom, end_datetime_zoom)
    ax_bot.set_ylabel("Total rainfall per one-third month [mm]")
    ax_bot.set_xlabel("Time (one-third month resolution)")
    ax_bot.grid()
    ax_bot.set_title("Zoomed-in view")

    return fig, (ax_top, ax_bot)
