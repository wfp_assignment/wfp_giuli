"""
Codes related to the WFP rainfall prediction assignment.

This is a bare-bone API documentation.

# Useful links

The code repository is [here](https://gitlab.com/wfp_assignment/wfp_giuli).

"""


import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
