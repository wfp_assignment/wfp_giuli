"""
Running autoregressive model analyses.
"""
import os
import pandas as pd

from wfp_giuli import ROOT_DIR
from wfp_giuli.ar_utils import (
    fit_ar_model_and_predict,
    fit_and_plot_ar_model,
)

DATA_DIR = os.path.join(os.path.dirname(ROOT_DIR), "data")


if __name__ == "__main__":

    df_analysis = pd.read_pickle(os.path.join(DATA_DIR, "df_analysis.pickle"))

    # region_ids = df_analysis.AdminCode.unique()
    region_ids = [102, 1017]

    # ===============================================
    # FIT AND PLOT AR MODELS: visual inspection
    # ===============================================
    figs = []
    for ii, region_id in enumerate(region_ids):
        print("-" * 50)
        print(f"region id: {region_id}; {ii+1}/{len(region_ids)}")
        fig = fit_and_plot_ar_model(region_id=region_id, log_and_shift_transform=True)
        figs.append(fig)

    # ===============================================
    # FIT AND PLOT AR MODELS: visual inspection
    # ===============================================

    df_preds = pd.DataFrame()
    for ii, region_id in enumerate(region_ids):
        print("-" * 50)
        print(f"region id: {region_id}; {ii+1}/{len(region_ids)}")
        df_pred = fit_ar_model_and_predict(region_id=region_id)
        df_preds = df_preds.append(df_pred)
