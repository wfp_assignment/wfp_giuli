"""
Fetch and pre-process data.
"""

# ============================================
# IMPORT REQUIRED LIBRARIES
# ============================================
import os
from datetime import datetime, timedelta
import math
from tabulate import tabulate

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import seaborn as sns

from wfp_giuli import ROOT_DIR


plt.interactive(False)

DATA_DIR = os.path.join(os.path.dirname(ROOT_DIR), "data")


def fetch_and_pre_process_data():
    # ============================================
    # FETCH THE DATA FROM PROVIDED URL
    # ============================================

    url = "https://wfp-public.oss-eu-central-1.aliyuncs.com/tmp/rainfall_by_country.csv"
    df = pd.read_csv(url)

    # ============================================
    # CREATE EQUALLY SPACED TIME STAMPS
    # ============================================
    df["day"] = df.decade.replace([1, 2, 3], [5, 15, 25])
    df["date"] = pd.to_datetime(df[["year", "month"]].assign(Day=df["day"]))

    first_year_analysis = df.date.min().year
    first_month_analysis = df.date.min().month
    last_year_analysis = df.date.max().year

    start_datetime = datetime(
        year=first_year_analysis, month=1, day=1, hour=0, minute=0, second=0
    )
    end_datetime = datetime(
        year=last_year_analysis, month=12, day=31, hour=23, minute=59, second=59
    )

    n_step = 3 * 10 * 12 * 3

    # we need an integer hour frequency as the smallest unit that `statsmodels` can deal with is 'H'
    # this introduces an offset to the middle of month-thirds, the code below minimizes this error
    diff_datetime = end_datetime - start_datetime

    total_hours = diff_datetime.days * 24 + diff_datetime.seconds / 3600
    step_hours = int(round(total_hours / (n_step - 1)))
    rem_time = timedelta(hours=total_hours - step_hours * (n_step - 1))

    updated_start_datetime = start_datetime + rem_time / 2
    dti_hours = pd.date_range(
        updated_start_datetime, periods=n_step, freq=f"{step_hours}H",
    )

    # -----
    step_datetime = diff_datetime / n_step
    step_datetime_days, step_datetime_seconds = (
        step_datetime.days,
        step_datetime.seconds,
    )
    step_datetime_hours = step_datetime_seconds // 3600
    step_datetime_seconds = step_datetime_seconds - step_datetime_hours * 3600

    initial_day = str(math.floor(step_datetime_days / 2)).zfill(2)
    initial_hour = str(math.floor(step_datetime_hours / 2)).zfill(2)

    dti = pd.date_range(
        start=f"{first_year_analysis}-{first_month_analysis}-{initial_day}-{initial_hour}",
        periods=n_step,
        freq=f"{step_datetime_days}D{step_datetime_hours}H{step_datetime_seconds}S",
    )
    # ============================================
    # PRELIMINARY CHECK OF THE DATASET
    # ============================================
    # Check number of regions
    admincodes = df.AdminCode.unique()
    number_of_regions = len(admincodes)

    # Check for negative values and replace with None
    df["mean_rainfall"][df.mean_rainfall < 0] = float("nan")
    number_inconsistent_values = len(df) - df["mean_rainfall"].count()

    # Check for double entries and clean-up database
    number_of_duplicates = len(df[df.duplicated()])
    df = df.drop_duplicates()

    # Add 'NaN' for incomplete time series and make check_plot
    df_analysis = pd.DataFrame(
        columns=["time_stamp", "AdminCode", "mean_rainfall"],
        index=np.arange(start=0, stop=n_step * number_of_regions, step=1),
    )
    df_analysis["time_stamp"] = np.tile(dti, number_of_regions)
    df_analysis["AdminCode"] = np.repeat(admincodes, len(dti))
    idx = 0
    for aa, admincode in enumerate(admincodes):
        df_admincode = df[df.AdminCode == admincode].reset_index()
        df_analysis_admincode = df_analysis[
            df_analysis["AdminCode"] == admincode
        ].reset_index()
        if len(df_analysis_admincode) == len(df_admincode):
            df_analysis_admincode.loc[:, "mean_rainfall"] = df_admincode.loc[
                :, "mean_rainfall"
            ]
        else:
            for ii in range(len(df_analysis_admincode)):
                diff = abs(
                    df_analysis_admincode.loc[ii, "time_stamp"] - df_admincode["date"]
                )
                min_diff_seconds = min(diff).total_seconds()
                idx_min = diff.idxmin()
                if min_diff_seconds < 3600 * 24 * 5:
                    df_analysis_admincode.loc[ii, "mean_rainfall"] = df_admincode.loc[
                        idx_min, "mean_rainfall"
                    ]
                else:
                    df_analysis_admincode.loc[ii, "mean_rainfall"] = float("nan")
        df_analysis["mean_rainfall"][idx : idx + 1080] = df_analysis_admincode[
            "mean_rainfall"
        ]
        idx = idx + 1080

    df_analysis["time_stamp_dti_hours"] = np.tile(dti_hours, number_of_regions)
    df_analysis.index = np.tile(dti_hours, number_of_regions)
    df_without_nan = df_analysis.dropna()
    size_per_region = df_without_nan.groupby("AdminCode").size()
    regions_with_missing_values = size_per_region[size_per_region != 1080].index.values
    number_of_regions_with_missing_values = len(
        size_per_region[size_per_region != 1080]
    )

    df_incomplete = df_analysis[
        df_analysis["AdminCode"].isin(regions_with_missing_values)
    ].reset_index()

    df_plot_check = pd.DataFrame(columns=regions_with_missing_values, index=dti)

    for jj, rain in enumerate(df_incomplete.mean_rainfall):
        time_stamp = df_incomplete.loc[jj, "time_stamp"]
        region = df_incomplete.loc[jj, "AdminCode"]
        if math.isnan(rain):
            df_plot_check.loc[time_stamp, region] = 0
        else:
            df_plot_check.loc[time_stamp, region] = 1

    df_plot_check = df_plot_check[df_plot_check.columns].astype(float)
    df_plot_check.index = df_plot_check.index.strftime("%d %b %Y")

    # Heat-map, value = 1 if the 'mean_rainfall' variable is not NaN, otherwise value = 0
    sns.set(font_scale=0.8)
    myColors = ((0.8, 0.0, 0.0, 1.0), (0.0, 0.0, 0.8, 1.0))
    cmap = LinearSegmentedColormap.from_list("Custom", myColors, len(myColors))
    ax = sns.heatmap(data=df_plot_check, xticklabels=True, yticklabels=200, cmap=cmap)

    colorbar = ax.collections[0].colorbar
    colorbar.set_ticks([0.25, 0.75])
    colorbar.set_ticklabels(["NaN value", "Rainfall recorded"])

    plt.xlabel("AdminCode")
    plt.ylabel("Time-stamp")
    plt.show()

    # Remove the locations with less than 50% of the data
    regions_missing_more_than_50_percent_data = size_per_region[
        size_per_region < 1080 / 2
    ].index.values
    df_analysis = df_analysis[
        ~df_analysis["AdminCode"].isin(regions_missing_more_than_50_percent_data)
    ]

    # print summary of the database
    table = [
        ["Database summary", " "],
        ["Years of observation", last_year_analysis - first_year_analysis + 1],
        ["Number of observations per location", n_step],
        ["Total number of locations", number_of_regions],
        [
            "Number of locations with missing or inconsistent values",
            number_of_regions_with_missing_values,
        ],
        ["AdminCodes removed from analysis", regions_missing_more_than_50_percent_data],
    ]
    print(tabulate(table, headers="firstrow"))
    df_analysis["mean_rainfall"] = df_analysis["mean_rainfall"].astype(float)
    df_analysis.to_pickle(os.path.join(DATA_DIR, "df_analysis.pickle"))

    return
