"""
Autoregressive modelling related utilities.

TODO:
 * clean up the code
 * move model fit into one dedicated function that is reused
 * get rid of duplicate codes such as confidence interval prediction
"""
import os
import copy
from typing import Tuple

import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt
import pandas as pd
import statsmodels as sm
from statsmodels.tsa.ar_model import ar_select_order
from statsmodels.tsa.arima_process import arma2ma
import scipy as sp

from wfp_giuli import ROOT_DIR
from wfp_giuli.utils_ts import (
    int_to_datetime,
    visualize_time_series_fit,
)

plt.interactive(False)

DATA_DIR = os.path.join(os.path.dirname(ROOT_DIR), "data")
FIG_DIR = os.path.join(ROOT_DIR, "results")

# =========================================
# CONTROL AND OPTIONS
# =========================================


def fit_and_plot_ar_model(
    region_id: int,
    seasonal_model: bool = True,
    log_and_shift_transform: bool = True,
    shift: int = 1,
    confidence_interval_alpha: float = 0.05,
    first_portion_for_fit: float = 0.92,
    steps_short_forecast: int = 3,
    start_int_pred: int = 0,
    end_int_pred: int = 1100,
    max_lag: int = 100,
    missing_data_allowed: bool = True,
    save_plot: bool = True,
    make_plot: bool = True,
):
    model_name = "AutoReg"
    # =========================================
    # ANALYSIS
    # =========================================

    df_analysis = pd.read_pickle(os.path.join(DATA_DIR, "df_analysis.pickle"))

    figs = []

    # =========================================
    # GET DATA
    # =========================================
    df = df_analysis[df_analysis["AdminCode"] == region_id]
    df.index = df["time_stamp_dti_hours"]
    df.index.freq = "244H"

    all_ts_data = pd.Series(df["mean_rainfall"].values, index=df.index)
    if log_and_shift_transform:
        df_tr = copy.deepcopy(df)
        confidence_interval_warning = "approximate"
        df_tr.mean_rainfall = np.log(df_tr.mean_rainfall + shift)
        dt_tr = pd.Series(df_tr["mean_rainfall"].values, index=df_tr.index)
    else:
        dt_tr = pd.Series(df["mean_rainfall"].values, index=df.index)
        confidence_interval_warning = ""

    # =========================================
    # AUTOREGRESSION MODEL
    # =========================================

    # -----------------------------------------
    # Fit
    # -----------------------------------------
    end_int_fit = round(len(dt_tr) * first_portion_for_fit)
    ts_data_for_fit = dt_tr[:end_int_fit]
    n_dt_fit = len(ts_data_for_fit.index)

    fitted_model, ts_data_for_fit = fit_ar_model(
        ts_data=ts_data_for_fit,
        max_lag=max_lag,
        seasonal_model=seasonal_model,
        missing_data_allowed=missing_data_allowed,
    )
    fitted_model.summary()

    # -----------------------------------------
    # Post-process
    # -----------------------------------------
    # this madness is needed because pandas and `statsmodels` both want
    # to allocate a few dozens of GBs of memory for plotting a few kBs of data

    # Prediction
    start_datetime, end_datetime, freq = int_to_datetime(
        ts_data=dt_tr, start_int=start_int_pred, end_int=end_int_pred
    )
    mean_pred = fitted_model.predict(start=start_int_pred, end=end_int_pred)
    # mean_pred = fitted_model.predict(start=start_int_pred, end=end_int_pred, dynamic=True)
    times_pred = pd.date_range(start=start_datetime, end=end_datetime, freq=freq)

    # Forecast (because `statsmodels` is weird)
    steps_long_forecast = end_int_pred - n_dt_fit
    start_datetime, end_datetime, freq = int_to_datetime(
        ts_data=ts_data_for_fit, start_int=n_dt_fit, end_int=end_int_pred
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # WATCH OUT! - it is adapted from the source code of `statsmodels` to get the credible interval...
    predictions = mean_pred
    _, _, oos, _ = fitted_model.model._get_prediction_index(
        start_datetime, end_datetime
    )

    pred_oos = np.asarray(predictions)[-oos:]
    ar_params = fitted_model._lag_repr()
    ma = arma2ma(ar_params, [1], lags=oos)
    fc_error = np.sqrt(fitted_model.sigma2) * np.cumsum(ma ** 2)
    quantile = norm.ppf(confidence_interval_alpha / 2)
    lower = pred_oos + fc_error * quantile
    upper = pred_oos + fc_error * -quantile

    confidence_interval_forecast = np.vstack((lower, upper)).T
    # WATCH OUT!
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    times_forecast = pd.date_range(start=start_datetime, end=end_datetime, freq=freq)
    aic = fitted_model.aic

    # -----------------------------------------
    # Inverse transform - approximate confidence interval!
    # -----------------------------------------
    if log_and_shift_transform:
        mean_prediction = np.exp(mean_pred) - shift
        confidence_interval_forecast = np.exp(confidence_interval_forecast) - shift
    else:
        mean_prediction = copy.deepcopy(mean_pred)

    # Get R^2 adjusted from linear regression
    y_ground_truth = ts_data_for_fit.values
    y_pred = mean_prediction[ts_data_for_fit.index].values
    idx = np.logical_not(np.logical_or(np.isnan(y_ground_truth), np.isnan(y_pred)))
    _, _, r_squared, _, _ = sp.stats.linregress(y_ground_truth[idx], y_pred[idx])
    # =========================================
    # Visualize
    # =========================================
    if make_plot:
        # -----------------------------------------
        # Time-series plot
        # -----------------------------------------
        fig, _ = visualize_time_series_fit(
            all_ts_data=all_ts_data,
            ts_data_for_fit=ts_data_for_fit,
            times_pred=times_pred,
            mean_prediction=mean_prediction,
            steps_forecast=steps_short_forecast,
            times_forecast=times_forecast,
            confidence_interval_forecast=confidence_interval_forecast,
            confidence_interval_alpha=confidence_interval_alpha,
            confidence_interval_warning=confidence_interval_warning,
            aic=aic,
            r_squared=r_squared,
        )

        model_lag = fitted_model.ar_lags[-1]
        fig.suptitle(
            f"Region (AdminCode): {region_id}#model_name={model_name}(lag={model_lag:.0f})"
            f"#seasonal_model={seasonal_model}#transform={log_and_shift_transform}"
        )
        figs.append(fig)

        # Save
        if save_plot:
            analysis_ID = f"#region={region_id}#seasonal_model={seasonal_model}#transform={log_and_shift_transform}"
            fig.set_size_inches(16, 9)
            f_path = os.path.join(
                FIG_DIR, model_name, f"data_vs_fitted_model{analysis_ID}",
            )
            plt.savefig(f"{f_path}.pdf")
            plt.savefig(f"{f_path}.png")
            plt.close()

        # -----------------------------------------
        # Diagnostic plot
        # -----------------------------------------
        model_lag = fitted_model.ar_lags[-1]
        fig = plt.figure(figsize=(16, 9))
        fig = fitted_model.plot_diagnostics(fig=fig, lags=model_lag)
        figs.append(fig)

        # Save
        if save_plot:
            fig.set_size_inches(16, 9)
            f_path = os.path.join(
                FIG_DIR, model_name, f"fitted_model_diagnostics{analysis_ID}",
            )
            plt.savefig(f"{f_path}.pdf")
            plt.savefig(f"{f_path}.png")
            plt.close()

    return figs


def fit_ar_model_and_predict(
    region_id: int,
    seasonal_model: bool = True,
    log_and_shift_transform: bool = True,
    shift: int = 1,
    step_back_int_fit: float = 0,
    steps_forecast: int = 3,
    max_lag: int = 100,
    confidence_interval_alpha: float = 0.05,
    missing_data_allowed: bool = True,
):

    # =========================================
    # GET DATA
    # =========================================
    df_analysis = pd.read_pickle(os.path.join(DATA_DIR, "df_analysis.pickle"))

    df = df_analysis[df_analysis["AdminCode"] == region_id]
    df.index = df["time_stamp_dti_hours"]
    df.index.freq = "244H"

    if log_and_shift_transform:
        df_tr = copy.deepcopy(df)
        df_tr.mean_rainfall = np.log(df_tr.mean_rainfall + shift)
        dt_tr = pd.Series(df_tr["mean_rainfall"].values, index=df_tr.index)
    else:
        dt_tr = pd.Series(df["mean_rainfall"].values, index=df.index)

    # =========================================
    # AUTOREGRESSION MODEL
    # =========================================

    # -----------------------------------------
    # Fit
    # -----------------------------------------
    end_int_fit = int(len(dt_tr) - step_back_int_fit)
    ts_data_for_fit = dt_tr[:end_int_fit]

    fitted_model, ts_data_for_fit = fit_ar_model(
        ts_data=ts_data_for_fit,
        max_lag=max_lag,
        seasonal_model=seasonal_model,
        missing_data_allowed=missing_data_allowed,
    )
    fitted_model.summary()

    # -----------------------------------------
    # Post-process
    # -----------------------------------------

    # Prediction
    start_int_pred = end_int_fit
    end_int_pred = end_int_fit + steps_forecast - 1
    start_datetime_pred, end_datetime_pred, freq = int_to_datetime(
        ts_data=dt_tr, start_int=start_int_pred, end_int=end_int_pred
    )
    mean_prediction = fitted_model.predict(start=start_int_pred, end=end_int_pred)
    times_pred = pd.date_range(
        start=start_datetime_pred, end=end_datetime_pred, freq=freq
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # WATCH OUT! - it is adapted from the source code of `statsmodels` to get the credible interval...
    predictions = mean_prediction
    _, _, oos, _ = fitted_model.model._get_prediction_index(
        start_datetime_pred, end_datetime_pred
    )

    pred_oos = np.asarray(predictions)[-oos:]
    ar_params = fitted_model._lag_repr()
    ma = arma2ma(ar_params, [1], lags=oos)
    fc_error = np.sqrt(fitted_model.sigma2) * np.cumsum(ma ** 2)
    quantile = norm.ppf(confidence_interval_alpha / 2)
    lower = pred_oos + fc_error * quantile
    upper = pred_oos + fc_error * -quantile

    confidence_interval_forecast = np.vstack((lower, upper)).T
    # WATCH OUT!
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # -----------------------------------------
    # Inverse transform - approximate confidence interval!
    # -----------------------------------------
    if log_and_shift_transform:
        mean_prediction = np.exp(mean_prediction) - shift
        confidence_interval_forecast = np.exp(confidence_interval_forecast) - shift
    else:
        mean_prediction = copy.deepcopy(mean_prediction)

    df_pred = pd.DataFrame(
        {
            "region": region_id,
            "mean_prediction": mean_prediction,
            "confidence_interval_lower": confidence_interval_forecast[:, 0],
            "confidence_interval_upper": confidence_interval_forecast[:, 1],
        },
        index=times_pred,
    )

    return df_pred


def fit_ar_model(
    ts_data,
    seasonal_model: bool = True,
    max_lag: int = 100,
    missing_data_allowed: bool = True,
) -> Tuple[sm.tsa.ar_model.AutoReg, pd.core.series.Series]:
    # -----------------------------------------
    # PRE-PROCESS
    # -----------------------------------------
    # check for missing values
    mask_missing = ts_data.isna()
    if mask_missing.any():
        if not missing_data_allowed:
            raise ValueError(
                "Missing values are not allowed in the time-series!"
                "You can allow for them by using `missing_data_allowed=True`. "
                "It linearly interpolates or extrapolates a value and replaces "
                "the missing value."
            )
        else:
            # since our points are equally spaced in time
            ts_data.interpolate(method="linear", limit_area=None, inplace=True)
            print(
                f"\nWARNING: {mask_missing.sum()} missing data points are linearly interpolated "
                f"(including extrapolation) based on the surrounding points. \n"
                f"Times (index) of missing points with the interpolated values: \n"
                f"{ts_data[mask_missing]} \n"
            )

    # -----------------------------------------
    # HYPER PARAMETER OPTIMIZIATION
    # -----------------------------------------
    # selection of the optimal number of lags
    sel = ar_select_order(
        ts_data, maxlag=max_lag, ic="aic", seasonal=seasonal_model, missing="none"
    )
    model_lag = sel.ar_lags[-1]
    if model_lag == max_lag:
        print(
            f"WARNING: The optimal lag equals to the maximum allowed lag ({max_lag}). Consider increasing `max_lag`."
        )
    print(f"Optimal lag: {model_lag}")

    # -----------------------------------------
    # FIT MODEL
    # -----------------------------------------
    model = sel.model
    fitted_model = model.fit()
    return fitted_model, ts_data
