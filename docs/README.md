This folder is kept for local builds that are not added to the repository.

Local webserver:
```
pdoc --config latex_math=True --http : wfp_giuli
```

Local html build:

```
pdoc --html wfp_giuli --force --output-dir docs
```