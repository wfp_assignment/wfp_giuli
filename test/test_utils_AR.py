"""
TODO:
 * write tests for all code

"""
import pytest

from wfp_giuli.utils_ts import int_to_datetime


def test_int_to_datetime_wrong_input():
    with pytest.raises(TypeError):
        int_to_datetime(ts_data=12, start_int=0, end_int=10)
